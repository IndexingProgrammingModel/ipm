/*
 Copyright (c) 2013 UChicago Argonne, LLC

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <ipm.h>

/*
    This code demonstrates the use of the IPM parallel programming model with some simple examples.

    See ipm.h and IPM.pdf for more details on the model.

    The subroutines below that begin with k are user provided "kernel" or "task" functions. They work on a piece of data provided
  by IPM and have no parallelism within them. 

    The "main" user program creates empty IPM objects (such as IPM_ArrayNumeric) and then "launches" IPM to run the kernel/task functions on 
  the data structure. The kernel/task functions add data to the IPM objects which then "grow", later calls to the kernel/task functions
  may use more processes/threads then earlier because the data structures are larger.

*/

/*
    This kernel/task is called on a completely empty set of IPM objects and puts two double precision entries in the u object
  and a single "element", consisting of the "addresses" (called indices) of those two entries, into the e object.
*/
void kCreate(IPMK_ArrayNumeric u,IPMK_ArrayIndex e,IPMK_Error *err)
{
  if (u->offset->ptotal > 0) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);
  if (e->offset->ptotal > 0) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);

  //   Build a u with two indices and an element with one index (element)
  double x[] = {0.0,1.0};
  IPMK_Index points[2];
  points[0] = IPMK_ArrayNumericAdd(u,1,&x[0],err);
  points[1] = IPMK_ArrayNumericAdd(u,1,&x[1],err);
  IPMK_ArrayIndexAdd(e,2,points,err);
}

/*
   This kernel/task splits each element it is provided into two elements by introducing a new double precision entry at its mid-point
*/
void kRefine(IPMK_ArrayNumeric u,IPMK_ArrayIndex e,IPMK_Error *err)
{
  if (e->dataoffset != u->offset) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);
  for (IPMK_Index p=IPMK_ArrayIndexStart(e,err); p<IPMK_ArrayIndexEnd(e,err); p++) {
    const IPMK_Index *ps = IPMK_ArrayIndexGet(e,p,err);
    double     xlow  = *IPMK_ArrayNumericGet(u,ps[0],err);
    double     xhigh = *IPMK_ArrayNumericGet(u,ps[1],err);
    double     xmid  = (xlow + xhigh)/2.0;

    // add new index in u and new index (element) in elements
    IPMK_Index points[2];
    points[0] = IPMK_ArrayNumericAdd(u,1,&xmid,err);
    points[1] = ps[1];
    IPMK_ArrayIndexAdd(e,2,points,err);

    // change current element in elements to use new u index as left value
    IPMK_Index npoints[2];
    npoints[0] = ps[0];
    npoints[1] = points[0];
    IPMK_ArrayIndexChange(e,p,2,npoints,err);
  }
}

void kPrintMatrix(IPMK_ArrayNumeric A,IPMK_ArrayIndex Au,IPMK_ArrayIndex Av,IPMK_Error *err)
{
  IPMK_Index p;

  for (p=IPMK_ArrayNumericStart(A,err); p<IPMK_ArrayNumericEnd(A,err); p++) {
    printf("row %d column %d value %g \n",(int)*IPMK_ArrayIndexGet(Av,p,err),(int)*IPMK_ArrayIndexGet(Au,p,err),*IPMK_ArrayNumericGet(A,p,err));
  }
  fflush(stdout);
}

void kPrintElements(IPMK_ArrayNumeric u,IPMK_ArrayIndex e,IPMK_Error *err)
{
  for (IPMK_Index p=IPMK_ArrayIndexStart(e,err); p<IPMK_ArrayIndexEnd(e,err); p++) {
    IPMK_Index *ps = IPMK_ArrayIndexGet(e,p,err);
    printf("%d -- %d %d  %g %g\n",(int)p,(int)ps[0],(int)ps[1],*IPMK_ArrayNumericGet(u,ps[0],err),*IPMK_ArrayNumericGet(u,ps[1],err));
  }
  fflush(stdout);
}

/*
    Thus kernel/task computes the sum of all its entries of the IPM_ArrayNumeric u and stores the result in the IPM_RArray s.
  It's context is created below with 

  IPM_FunctionCreate(IPM_FUNCTION_KERNEL,kUSum,2,IPM_MEMORY_READ,IPM_MEMORY_ADD,&err);

  The IPM_MEMORY_ADD flag means that IPM will sum the entries created by each task in s together (s has only a single value for each task)
  This summation is much like an MPI_Allreduce()

  Note that each kernel/task has its own "copy" of s, but when s is propogated back to the main program it only has a single "copy" whose
  value in this case is the sum of the values from all tasks.

*/
void kUSum(IPMK_ArrayNumeric u,IPMK_RArray s,IPMK_Error *err)
{
  double     sum = 0;

  for (IPMK_Index p=IPMK_ArrayNumericStart(u,err); p<IPMK_ArrayNumericEnd(u,err); p++) {
    sum += *IPMK_ArrayNumericGet(u,p,err);
  }
  *IPMK_RArrayGet(s,err) = sum;
}

// For this u we know the exact layout of ddata and so can access values directly 
void kUSumOptimized(IPMK_ArrayNumeric u,IPMK_RArray s,IPMK_Error *err)
{
  int        i,n = IPMK_ArrayNumericEnd(u,err) - IPMK_ArrayNumericStart(u,err);
  double     sum = 0,*ddata = IPMK_ArrayNumericGet(u,u->offset->pstart[Currentthread],err);

  for (i=0; i<n; i++) {
    sum += ddata[i];
  }
  *IPMK_RArrayGet(s,err) = sum;
}

// can do optimized version of this as well
void kElementSum(IPMK_ArrayNumeric u,IPMK_ArrayIndex e,IPMK_RArray r,IPMK_Error *err)
{
  if (IPMK_ArrayIndexGetDataOffset(e,err) != IPMK_ArrayNumericGetOffset(u,err)) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);
  double    sum = 0;

  for (IPMK_Index p=IPMK_ArrayIndexStart(e,err); p<IPMK_ArrayIndexEnd(e,err); p++) {
    IPMK_Index *ps = IPMK_ArrayIndexGet(e,p,err);
    sum += *IPMK_ArrayNumericGet(u,ps[0],err);
    sum += *IPMK_ArrayNumericGet(u,ps[1],err);
  }
  *IPMK_RArrayGet(r,err) = sum;
}

//  u and v need same layout 
void kAxpy(IPMK_ArrayNumeric u,IPMK_ArrayNumeric v,IPMK_RArray r,IPMK_Error *err)
{
  if (IPMK_ArrayNumericGetOffset(u,err) != IPMK_ArrayNumericGetOffset(v,err)) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);
  double    alpha = *IPMK_RArrayGet(r,err);

  for (IPMK_Index p=IPMK_ArrayNumericStart(u,err); p<IPMK_ArrayNumericEnd(u,err); p++) {
    *IPMK_ArrayNumericGet(v,p,err) += alpha* *IPMK_ArrayNumericGet(u,p,err);
  }
}

void kUSet(IPMK_ArrayNumeric u,IPMK_RArray s,IPMK_Error *err)
{
  for (IPMK_Index p=IPMK_ArrayNumericStart(u,err); p<IPMK_ArrayNumericEnd(u,err); p++) {
    *IPMK_ArrayNumericGet(u,p,err) = *IPMK_RArrayGet(s,err);
  }
}

/*
   Sums the nodal values from u to v. This is meaningless but has same memory access as 
  finite element function evaluation

  This will not work in shared memory threaded environment since two different threads may be
  updating the same grid point at the same time!

   Have to some let v know that ghosted v values need separate location for each thread and then
   PostLaunch() code combines them together. 

*/
void kFiniteElementAssembly(IPMK_ArrayIndex e,IPMK_ArrayNumeric A, IPMK_ArrayIndex Au,IPMK_ArrayIndex Av,IPMK_Error *err)
{
  for (IPMK_Index p=IPMK_ArrayIndexStart(e,err); p<IPMK_ArrayIndexEnd(e,err); p++) {
    IPMK_Index *ps = IPMK_ArrayIndexGet(e,p,err);
    IPMK_ArrayIndexAdd(Au,1,&ps[0],err);
    IPMK_ArrayIndexAdd(Av,1,&ps[1],err);
    double value[1] = {10.0};
    IPMK_ArrayNumericAdd(A,1,value,err);
  }
}

/* 
   Kernel/task that performs part of a sparse matrix vector product    y = A * x

   A, ix, and iy need the same [pstart,pend) how do we insure that?
*/
void kMult(IPMK_ArrayNumeric x, IPMK_ArrayNumeric y,IPMK_ArrayNumeric A,IPMK_ArrayIndex ix,IPMK_ArrayIndex iy,IPMK_Error *err)
{
  for (IPMK_Index p=IPMK_ArrayIndexStart(ix,err); p<IPMK_ArrayIndexEnd(ix,err); p++) {
    IPMK_Index px = *IPMK_ArrayIndexGet(ix,p,err);
    IPMK_Index py = *IPMK_ArrayIndexGet(iy,p,err);
    *IPMK_ArrayNumericGet(y,py,err) += *IPMK_ArrayNumericGet(A,p,err)**IPMK_ArrayNumericGet(x,px,err);
  }
}

int main(int argc,char **args)
{
  IPM_Error      err;

  IPM_Initialize(&argc,&args,&err);
  
  IPM_ArrayNumeric u        = IPM_ArrayNumericCreate(&err);
  IPM_ArrayIndex   elements = IPM_ArrayIndexCreate(&err);
  IPM_ArrayIndexSetOffset(elements,IPM_ArrayNumericGetOffset(u,&err),&err);    //  element indices are into the u object 
  
  IPM_Function myCreate = IPM_FunctionCreate(IPM_FUNCTION_REPARTITION,kCreate,2,IPM_MEMORY_WRITE,IPM_MEMORY_WRITE,&err);
  IPM_Launch(myCreate,(IPM_Array)u,elements,&err);

  IPM_Launch(ipm_arrayprint1,(IPM_Array)u,&err);
  IPM_Launch(ipm_arrayprint2,(IPM_Array)u,elements,&err);

  IPM_Function myRefine = IPM_FunctionCreate(IPM_FUNCTION_REPARTITION,kRefine,2,IPM_MEMORY_WRITE,IPM_MEMORY_WRITE,&err);
  IPM_Launch(myRefine,(IPM_Array)u,elements,&err);

  IPM_Launch(ipm_arrayprint1,(IPM_Array)u,&err);
  IPM_Launch(ipm_arrayprint2,(IPM_Array)u,elements,&err);

  IPM_Launch(myRefine,(IPM_Array)u,elements,&err);

  IPM_Launch(ipm_arrayprint1,(IPM_Array)u,&err);
  IPM_Launch(ipm_arrayprint2,(IPM_Array)u,elements,&err);

  IPM_Launch(myRefine,(IPM_Array)u,elements,&err);

  IPM_Launch(ipm_arrayprint1,(IPM_Array)u,&err);
  IPM_Launch(ipm_arrayprint2,(IPM_Array)u,elements,&err);

  IPM_Launch(myRefine,(IPM_Array)u,elements,&err);

  IPM_Launch(ipm_arrayprint1,(IPM_Array)u,&err);
  IPM_Launch(ipm_arrayprint2,(IPM_Array)u,elements,&err);
  
  IPM_RArray sum = IPM_RArrayCreate(1,&err);
  IPM_Function myUSum = IPM_FunctionCreate(IPM_FUNCTION_KERNEL,kUSum,2,IPM_MEMORY_READ,IPM_MEMORY_ADD,&err);
  IPM_Launch(myUSum,(IPM_Array)u,sum,&err);
  IPM_Printf("U sum %g\n",*IPM_RArrayGet(sum,&err));

  IPM_Function myUSumOptimized = IPM_FunctionCreate(IPM_FUNCTION_KERNEL,kUSumOptimized,2,IPM_MEMORY_READ,IPM_MEMORY_ADD,&err);
  IPM_Launch(myUSumOptimized,(IPM_Array)u,sum,&err);
  IPM_Printf("Optimized U sum %g\n",*IPM_RArrayGet(sum,&err));

  IPM_Function myElementSum = IPM_FunctionCreate(IPM_FUNCTION_KERNEL,kElementSum,3,IPM_MEMORY_READ,IPM_MEMORY_READ,IPM_MEMORY_ADD,&err);
  IPM_Launch(myElementSum,(IPM_Array)u,elements,sum,&err);
  IPM_Printf("Element sum %g\n",*IPM_RArrayGet(sum,&err));

  IPM_ArrayNumeric v = IPM_ArrayNumericClone(u,&err);
  IPM_ArrayNumericCopy(u,v,&err);

  IPM_RArray zero = IPM_RArrayCreate(1,&err);
  *IPM_RArrayGet(zero,&err) = 0.0;
  IPM_Function myUSet = IPM_FunctionCreate(IPM_FUNCTION_KERNEL,kUSet,2,IPM_MEMORY_WRITE,IPM_MEMORY_READ,&err);
  IPM_Launch(myUSet,(IPM_Array)v,zero,&err);

  IPM_RArray alpha = IPM_RArrayCreate(1,&err);
  *IPM_RArrayGet(alpha,&err) = 2.0;
 
  IPM_Function myAxpy = IPM_FunctionCreate(IPM_FUNCTION_KERNEL,kAxpy,3,IPM_MEMORY_WRITE,IPM_MEMORY_READ,IPM_MEMORY_READ,&err);
  IPM_Launch(myAxpy,(IPM_Array)u,v,alpha,&err);

  IPM_Launch(ipm_arrayprint1,(IPM_Array)u,&err);

  IPM_ArrayNumeric A;
  A = IPM_ArrayNumericCreate(&err);
  IPM_ArrayIndex Au,Av;
  Au = IPM_ArrayIndexCreate(&err); IPM_ArrayIndexSetOffset(Au,u->offset,&err); 
  Av = IPM_ArrayIndexCreate(&err); IPM_ArrayIndexSetOffset(Av,v->offset,&err); 

  // fill up A, Au and Av

  IPM_Function myFiniteElementAssembly = IPM_FunctionCreate(IPM_FUNCTION_KERNEL,kFiniteElementAssembly,4,IPM_MEMORY_READ,IPM_MEMORY_WRITE,IPM_MEMORY_WRITE,IPM_MEMORY_WRITE,&err);
  IPM_Launch(myFiniteElementAssembly,(IPM_Array)elements,A,Au,Av,&err);

  IPM_Launch(ipm_arrayprint1,(IPM_Array)A,&err);
  IPM_Launch(ipm_arrayprint2,(IPM_Array)u,Au,&err);
  IPM_Launch(ipm_arrayprint2,(IPM_Array)v,Av,&err);
  IPM_Printf("Matrix\n",&err);
  IPM_Function myPrintMatrix = IPM_FunctionCreate(IPM_FUNCTION_SEQUENTIAL,kPrintMatrix,3,IPM_MEMORY_READ,IPM_MEMORY_READ,IPM_MEMORY_READ,&err);
  IPM_Launch(myPrintMatrix,(IPM_Array)A,Au,Av,&err);

  IPM_Launch(myUSet,(IPM_Array)v,zero,&err);
  IPM_Function myMult = IPM_FunctionCreate(IPM_FUNCTION_SEQUENTIAL,kMult,5,IPM_MEMORY_READ,IPM_MEMORY_READ,IPM_MEMORY_READ,IPM_MEMORY_READ,IPM_MEMORY_WRITE,&err);
  IPM_Launch(myMult,(IPM_Array)u,v,A,Au,Av,&err);

  IPM_Launch(ipm_arrayprint1,(IPM_Array)u,&err);

  IPM_Finalize(&err);
  return 0;
}
