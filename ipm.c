/*
 Copyright (c) 2013 UChicago Argonne, LLC

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <unistd.h>
#include <mpi.h>
#include <ipm.h>

int IPMU_AbortOnError = 0;

void IPMU_AttachDebugger(const char programname[],IPM_Error *err)
{
  int child = 0;

  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  if (!rank) {

    child = (int)fork();
    if (child < 0) {
      *err = IPM_ERROR_SYSTEM;
      return;
    }

    /*
      Swap role the parent and child. This is (I think) so that control c typed
      in the debugger goes to the correct process.
    */
    if (child) child = 0;
    else       child = getppid();
    
    if (child) { /* I am the parent, will run the debugger */
      const char *args[10];
      char       pid[10];
      int        j = 0;
      
      /*
	We need to send a continue signal to the "child" process on the
	alpha, otherwise it just stays off forever
      */
      sprintf(pid,"%d",(int)child);
      args[j++] = "xterm";
      args[j++] = "-e";
      args[j++] = "lldb";
      args[j++] = programname;
      args[j++] = "-p"; 
      args[j++] = pid; 
      args[j++] = 0;
      printf("[%d] %s %s %s %s %s %s \n",rank,args[0],args[1],args[2],args[3],args[4],args[5]);
      if (execvp(args[0],(char**)args)  < 0) {
	perror("Unable to start debugger in xterm");
	*err = IPM_ERROR_SYSTEM;
      }
    } else {   /* I am the child, continue with user code */
      sleep(5);
    }
  }
  MPI_Barrier(MPI_COMM_WORLD);
}

void IPM_ArrayIndexPrint(IPMK_ArrayNumeric u,IPMK_Error *err)
{
  IPMK_Index p;
  
  int rank; 
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  printf("[%d,%d] Data\n",rank,Currentthread);
  for (p=IPMK_ArrayNumericStart(u,err); p<IPMK_ArrayNumericEnd(u,err); p++) {
    int i;
    double *uu = IPMK_ArrayNumericGet(u,p,err);
    printf(" index global %d local %d \n",(int)u->offset->global[p-u->offset->pstart[0]],(int)(p-u->offset->pstart[0]));
    for (i=0; i<IPMK_ArrayNumericGetLength(u,p,err); i++) {
      printf("                            %d -- %g \n",i,uu[i]);
    }
  }
  if (Currentthread == 0) {
    printf("  Ghost values\n");
    for (p=u->offset->ghoststart; p<u->offset->ghostend; p++) {
      int i;
      double *uu = IPMK_ArrayNumericGet(u,p,err);
      printf(" index global %d local %d \n",(int)u->offset->global[p-u->offset->pstart[0]],(int)(p-u->offset->pstart[0]));
      for (i=0; i<IPMK_ArrayNumericGetLength(u,p,err); i++) {
	printf("                            %d -- %g \n",i,uu[i]);
      }
    }
  }
  fflush(stdout);
  *err = 0;
}

void IPM_ArrayNumericIndexPrint(IPMK_ArrayNumeric u,IPMK_ArrayIndex e,IPMK_Error *err)
{
  IPMK_Index p;
  
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  printf("[%d,%d] Idata\n",rank,Currentthread);
  for (p=IPMK_ArrayIndexStart(e,err); p<IPMK_ArrayIndexEnd(e,err); p++) {
    int i;
    IPMK_Index *ee = IPMK_ArrayIndexGet(e,p,err);
    printf(" index global %d local %d \n",(int)e->offset->global[p-e->offset->pstart[0]],(int)(p-e->offset->pstart[0]));
    for (i=0; i<IPMK_ArrayIndexGetLength(e,p,err); i++) {
      printf("                            %d -- global %d local %d\n",i,(int)(u->offset->global[ee[i] - u->offset->pstart[0]]),(int)(ee[i] - u->offset->pstart[0]));
    }
  }
  fflush(stdout);
  *err = 0;
}

IPM_Function ipm_arrayprint1 = NULL,ipm_arrayprint2 = NULL;
int Numthreads = 1;
int Currentthread = 0;

void IPM_Initialize(int *argc,char ***args,IPM_Error *err)
{
  MPI_Init(argc,args);
  for (int i=1; i<*argc; i++) {
    if (!strcmp((*args)[i],"--debugger")) {
      IPMU_AttachDebugger((*args)[0],err);
      IPMU_AbortOnError = 1;
    }
  }
  for (int i=1; i<*argc-1; i++) {
    if (!strcmp((*args)[i],"--threads")) {
      sscanf((*args)[i+1],"%d",&Numthreads);
      if (Numthreads > MAXTHREAD) IPM_SetErrorVoid(IPM_ERROR_INDEX_OUT_OF_RANGE);
    }
  }
  ipm_arrayprint1 = IPM_FunctionCreate(IPM_FUNCTION_SEQUENTIAL,IPM_ArrayIndexPrint,1,IPM_MEMORY_READ,err);
  ipm_arrayprint2 = IPM_FunctionCreate(IPM_FUNCTION_SEQUENTIAL,IPM_ArrayNumericIndexPrint,2,IPM_MEMORY_READ,IPM_MEMORY_READ,err);
  *err = 0;
}

void IPM_Finalize(IPM_Error *err)
{
  *err = MPI_Finalize();
}

void IPM_Printf(const char format[],...)
{
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  if (!rank) {
    va_list Argp;
    va_start(Argp,format);
    vfprintf(stdout,format,Argp);
    //IPM_Error *err = (IPM_Error*) va_arg(Argp, IPM_Error*);
    //    *err = IPM_ERROR_NONE;
    va_end(Argp);
  }
  // does not set err on other processes
}



void IPMU_SequentialPhaseBegin(IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  int        rank,size,tag = 0;
  MPI_Status status;

  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&size);
  if (Currentthread == 0) {
    if (size > 1 && rank) {
      MPI_Recv(0,0,MPI_INT,rank-1,tag,MPI_COMM_WORLD,&status);
    }
  }
}

void IPMU_SequentialPhaseEnd(IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  int    rank,size,tag = 0;
  MPI_Status     status;

  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&size);

  /* Send to the first process in the next group */
  if (Currentthread == 0) {
    if (size > 1) {
      MPI_Send(0,0,MPI_INT,(rank + 1) % size,tag,MPI_COMM_WORLD);
      if (!rank) {
	MPI_Recv(0,0,MPI_INT,size-1,tag,MPI_COMM_WORLD,&status);
      }
    }
  }
}


IPM_RArray IPM_RArrayCreate(int64_t len,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  IPM_RArray data = (IPM_RArray) malloc(sizeof(struct _p_IPM_RArray));
  if (!data) IPM_SetError(IPM_ERROR_OUT_OF_MEMORY,NULL);
  data->type  = IPM_REDUNDANT;
  data->length    = len;
  for (int i=0; i<Numthreads; i++) {
    data->ddata[i]     = (double *) malloc(len*sizeof(double));
    if (!data->ddata[i]) IPM_SetError(IPM_ERROR_OUT_OF_MEMORY,NULL);
  }
  return data;
}

void IPMU_RArrayReduce(IPM_RArray data,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  double *tmp = malloc(data->length*sizeof(double));
  if (!tmp) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  for (int j=0; j<data->length; j++) {
    tmp[j] =  0.0;
  }
  for (int i=0; i<Numthreads; i++) {
    for (int j=0; j<data->length; j++) {
      tmp[j] +=  data->ddata[i][j];
    }
  }
  MPI_Allreduce(tmp,data->ddata[0],data->length,MPI_DOUBLE,MPI_SUM, MPI_COMM_WORLD);
  free(tmp);
}

void IPMU_RArrayZero(IPM_RArray data,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  for (int i=0; i<Numthreads; i++) {
    memset(data->ddata[i],0,data->length*sizeof(double));
  }
}

void IPM_RArrayDestroy(IPM_RArray *data,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  *data = NULL;
}

#define IPM_THREADSPLIT 40
#define IPM_SPLIT  1000   // max number of spaces available per process

/*
    When Indices data on a IPM_ArrayIndex reference entrees in another process they are automatically mapped to index into the ghost
   locations of that kernels data, which follows the non-ghosted. Thus the "global" indices the task sees have no meaning outside of that 
   task.
*/

void IPM_ArrayNumericDestroy(IPM_ArrayNumeric *data,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  *data = NULL;
}

void IPM_ArrayIndexDestroy(IPM_ArrayIndex *data,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  *data = NULL;
}

IPM_ArrayIndex IPM_ArrayIndexCreate(IPM_Error *err)
{
  *err = IPM_ERROR_NONE; 
  IPM_ArrayIndex data = (IPM_ArrayIndex) malloc(sizeof(struct _p_IPM_ArrayIndex));
  data->type  = IPM_INDEX;
  if (!data) IPM_SetError(IPM_ERROR_OUT_OF_MEMORY,NULL);
  data->lenmax    = IPM_SPLIT;
  data->idata     = (IPMK_Index *) malloc(data->lenmax*sizeof(IPMK_Index));
  if (!data->idata) IPM_SetError(IPM_ERROR_OUT_OF_MEMORY,NULL);
  data->dataoffset = NULL;

  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  data->offset             = (IPM_Offset) malloc(sizeof(struct _p_IPM_Offset));
  if (!data->offset) IPM_SetError(IPM_ERROR_OUT_OF_MEMORY,NULL);

  data->offset->ghoststart = data->offset->pstart[0] + IPM_SPLIT/2;
  data->offset->ghostend   = data->offset->ghoststart;
  data->offset->pmax       = IPM_SPLIT;
  data->offset->offset     = (int64_t *) malloc(data->offset->pmax*sizeof(int64_t));
  if (!data->offset->offset) IPM_SetError(IPM_ERROR_OUT_OF_MEMORY,NULL);
  data->offset->length     = (int64_t *) malloc(data->offset->pmax*sizeof(int64_t));
  if (!data->offset->length) IPM_SetError(IPM_ERROR_OUT_OF_MEMORY,NULL);
  data->offset->global     = (IPMK_Index *) malloc(data->lenmax*sizeof(IPMK_Index));
  if (!data->offset->global) IPM_SetError(IPM_ERROR_OUT_OF_MEMORY,NULL);
  data->offset->movedlocation  = (IPMK_Index *) malloc(data->lenmax*sizeof(IPMK_Index));
  if (!data->offset->movedlocation) IPM_SetError(IPM_ERROR_OUT_OF_MEMORY,NULL);
  data->offset->offset[0]  = 0;

  for (int i=0; i<MAXTHREAD; i++) {
    data->offset->pstart[i]   = rank*IPM_SPLIT;
    data->offset->pend[i]     = rank*IPM_SPLIT;
    data->offset->pnewstart[i]     = data->offset->pend[i] + i*IPM_THREADSPLIT;
    data->offset->pnewend[i]       = data->offset->pnewstart[i];
  }
  for (int i=1; i<MAXTHREAD; i++) {
    data->offset->offset[data->offset->pnewstart[i]-data->offset->pstart[0]] =  data->offset->offset[data->offset->pnewstart[i-1]-data->offset->pstart[0]] + IPM_THREADSPLIT;
  }


  data->offset->ptotal     = 0;
  data->offset->context    = NULL;
  for (int i=0; i<MAXTHREAD; i++) {
    data->offset->kcontext[i]   = NULL;
  }
  return data;
}

IPM_ArrayNumeric IPM_ArrayNumericCreate(IPM_Error *err)
{
  *err = IPM_ERROR_NONE; 
  IPM_ArrayNumeric data = (IPM_ArrayNumeric) malloc(sizeof(struct _p_IPM_ArrayNumeric));
  if (!data) IPM_SetError(IPM_ERROR_OUT_OF_MEMORY,NULL);
  data->type  = IPM_NUMERIC;
  data->lenmax    = IPM_SPLIT;
  data->ddata     = (double *) malloc(data->lenmax*sizeof(double));
  if (!data->ddata) IPM_SetError(IPM_ERROR_OUT_OF_MEMORY,NULL);
  data->dataoffset = NULL;

  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  data->offset             = (IPM_Offset) malloc(sizeof(struct _p_IPM_Offset));
  if (!data->offset) IPM_SetError(IPM_ERROR_OUT_OF_MEMORY,NULL);

  data->offset->ghoststart = data->offset->pstart[0] + IPM_SPLIT/2;
  data->offset->ghostend   = data->offset->ghoststart;
  data->offset->pmax       = IPM_SPLIT;
  data->offset->offset     = (int64_t *) malloc(data->offset->pmax*sizeof(int64_t));
  if (!data->offset->offset) IPM_SetError(IPM_ERROR_OUT_OF_MEMORY,NULL);
  data->offset->length     = (int64_t *) malloc(data->offset->pmax*sizeof(int64_t));
  if (!data->offset->length) IPM_SetError(IPM_ERROR_OUT_OF_MEMORY,NULL);
  data->offset->global     = (IPMK_Index *) malloc(data->lenmax*sizeof(IPMK_Index));
  if (!data->offset->global) IPM_SetError(IPM_ERROR_OUT_OF_MEMORY,NULL);
  data->offset->movedlocation  = (IPMK_Index *) malloc(data->lenmax*sizeof(IPMK_Index));
  if (!data->offset->movedlocation) IPM_SetError(IPM_ERROR_OUT_OF_MEMORY,NULL);
  data->offset->offset[0]  = 0;

  for (int i=0; i<MAXTHREAD; i++) {
    data->offset->pstart[i]   = rank*IPM_SPLIT;
    data->offset->pend[i]     = rank*IPM_SPLIT;
    data->offset->pnewstart[i]     = data->offset->pend[i] + i*IPM_THREADSPLIT;
    data->offset->pnewend[i]       = data->offset->pnewstart[i];
  }
  for (int i=1; i<MAXTHREAD; i++) {
    data->offset->offset[data->offset->pnewstart[i]-data->offset->pstart[0]] =  data->offset->offset[data->offset->pnewstart[i-1]-data->offset->pstart[0]] + IPM_THREADSPLIT;
  }


  data->offset->ptotal     = 0;
  data->offset->context    = NULL;
  for (int i=0; i<MAXTHREAD; i++) {
    data->offset->kcontext[i]   = NULL;
  }
  return data;
}

//   Big trouble if you add data to 2 or more IPM_ArrayNumeric that have the same IPM_Offset
//   The add data methods are NOT suppose to change the currently available data at all, hence p is not changed, instead pnew is used
/*
    Each kernel can add new indices of data to a IPM_ArrayNumeric item without conflicting with each other; everything is suppose to be automatically merged afterwards in the IPMU_XXXPostLaunch()

    Returns the index at which the data can now be accessed. This index may be stored with IPMK_ArrayAddIndices() 

*/
IPMK_Index IPMK_ArrayNumericAdd(IPM_ArrayNumeric data,int64_t len,double *values,IPM_Error *err)
{
  memcpy(data->ddata + data->offset->offset[data->offset->pnewend[Currentthread] - data->offset->pstart[0]],values,len*sizeof(double));
  data->offset->length[data->offset->pnewend[Currentthread] - data->offset->pstart[0]] = len;
  data->offset->offset[data->offset->pnewend[Currentthread] - data->offset->pstart[0] + 1] = data->offset->offset[data->offset->pnewend[Currentthread] - data->offset->pstart[0]] + len;
  // won't work in really threaded environment
  int64_t g = data->offset->pnewend[Currentthread] - data->offset->pnewstart[Currentthread];
  for (int i=0; i<Currentthread; i++) g += data->offset->pnewend[i] - data->offset->pnewstart[i];
  g += data->offset->pnewstart[0];
  data->offset->global[data->offset->pnewend[Currentthread] - data->offset->pstart[0]] = g;
  return data->offset->pnewend[Currentthread]++;
}

IPMK_Index IPMK_ArrayIndexAdd(IPM_ArrayIndex data,int64_t len,IPMK_Index *values,IPM_Error *err)
{
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  //  printf("[%d] adding %d %d \n",rank,(int)values[0],(int)values[1]);
  memcpy(data->idata + data->offset->offset[data->offset->pnewend[Currentthread] - data->offset->pstart[0]],values,len*sizeof(IPMK_Index));
  data->offset->length[data->offset->pnewend[Currentthread] - data->offset->pstart[0]] = len;
  data->offset->offset[data->offset->pnewend[Currentthread] - data->offset->pstart[0] + 1] = data->offset->offset[data->offset->pnewend[Currentthread] - data->offset->pstart[0]] + len;
  int64_t g = data->offset->pnewend[Currentthread] - data->offset->pnewstart[Currentthread];
  for (int i=0; i<Currentthread; i++) g += data->offset->pnewend[i] - data->offset->pnewstart[i];
  g += data->offset->pnewstart[0];
  data->offset->global[data->offset->pnewend[Currentthread] - data->offset->pstart[0]] = g;
  return data->offset->pnewend[Currentthread]++;
}

// len must be same as before and index must already exist  each thread can only change its own values
void IPMK_ArrayIndexChange(IPM_ArrayIndex data,IPMK_Index p,int64_t len,IPMK_Index *values,IPM_Error *err)
{
  if (len !=  data->offset->length[p - data->offset->pstart[0]]) IPM_SetErrorVoid(IPM_ERROR_WRONG_STATE);
  memcpy(data->idata + data->offset->offset[p - data->offset->pstart[0]],values,len*sizeof(IPMK_Index));
}

//   New data shares the offset with the old data; if one of them changes the offsets or adds to it then the other one is messed up
IPM_ArrayNumeric IPM_ArrayNumericClone(IPM_ArrayNumeric olddata,IPM_Error *err)
{
  IPM_ArrayNumeric data              = (IPM_ArrayNumeric) malloc(sizeof(struct _p_IPM_ArrayNumeric));
  if (!data) IPM_SetError(IPM_ERROR_OUT_OF_MEMORY,NULL);
  data->type      = olddata->type;
  data->lenmax    = olddata->lenmax;
  data->ddata     = (double *) malloc(data->lenmax*sizeof(double));
  if (!data->ddata) IPM_SetError(IPM_ERROR_OUT_OF_MEMORY,NULL);
  data->offset    = olddata->offset;
  data->dataoffset = NULL;
  *err = 0;
  return data;
}

//   New data shares the offset with the old data; if one of them changes the offsets or adds to it then the other one is messed up
IPM_ArrayIndex IPM_ArrayIndexClone(IPM_ArrayIndex olddata,IPM_Error *err)
{
  IPM_ArrayIndex data = (IPM_ArrayIndex) malloc(sizeof(struct _p_IPM_ArrayIndex));
  if (!data) IPM_SetError(IPM_ERROR_OUT_OF_MEMORY,NULL);
  data->type      = olddata->type;
  data->lenmax    = olddata->lenmax;
  data->idata     = (IPMK_Index *) malloc(data->lenmax*sizeof(IPMK_Index));
  if (!data->idata) IPM_SetError(IPM_ERROR_OUT_OF_MEMORY,NULL);
  data->offset    = olddata->offset;
  data->dataoffset = NULL;
  *err = 0;
  return data;
}

void IPM_ArrayNumericCopy(IPM_ArrayNumeric olddata,IPM_ArrayNumeric data,IPM_Error *err)
{
  // checks for compatibility
  memcpy(data->ddata,olddata->ddata,data->lenmax*sizeof(double));
  *err = 0;
}

void IPMU_FunctionPartition(IPM_Function,IPM_Error*);



/*
   This is called on the arguments before the tasks are called, it should ensure that each task has the data that it needs

   Ensures that all the entries in data[i] that are accessed by idata[i]->offsets[pstart,pend) are available to the task; i.e. any ghost indices needed are provided 

   Also if IPM_Memorytype for that data item is IPM_MEMORY_ADD makes sure that appropriate slots for each task are available so different tasks don't overwrite each other

*/
void IPMU_FunctionPreLaunch(IPM_Function func,IPM_Error *err)
{
  *err = 0;
}

/*
   This is called on the arguments after the tasks are called, it should ensure that any contributions from tasks that need to be merged are merged
*/
void IPMU_FunctionPostLaunch(IPM_Function func,IPM_Error *err)
{
  int q;
  for (q=0; q<func->n; q++) {
    IPM_Array data = func->array[q];
    if (data->type == IPM_INDEX || data->type == IPM_NUMERIC) {
      int rank; MPI_Comm_rank(MPI_COMM_WORLD,&rank);
      //  for (int i=0; i<data->offset->pend[0]-data->offset->pstart[0]; i++) {
      //  printf("[%d] i %d first first prefix data[2*i] %d\n",rank,2*i,(int)data->idata[2*i]);
      //  printf("[%d] i %d first first prefix data[2*i+1] %d\n",rank,2*i+1,(int)data->idata[2*i+1]);
      //  }}
      
      for (int j=data->offset->pstart[0]; j < data->offset->pend[Numthreads-1]; j++) {
        // these locations are not actually moved but we must still fill up the moved array
        data->offset->movedlocation[j - data->offset->pstart[0]] = j;
      }

      for (int j=data->offset->ghoststart; j < data->offset->ghostend; j++) {
        // these locations are not actually moved but we must still fill up the moved array
        // keep record of where each chunk is moved to 
        data->offset->movedlocation[j - data->offset->pstart[0]] = j;
      }

      for (int j=data->offset->pnewstart[0]; j < data->offset->pnewend[0]; j++) {
        // these locations are not actually moved but we must still fill up the moved array
        data->offset->movedlocation[j - data->offset->pstart[0]] = data->offset->pend[Numthreads-1] + j - data->offset->pnewstart[0];
      }

      //  copy all newly create data to contigious area at end of current local area 
      data->offset->pend[Numthreads-1] = data->offset->pnewend[0];
      for (int i=1; i<Numthreads; i++) {
        IPMK_Index len = 0;
        for (int j=data->offset->pnewstart[i]; j < data->offset->pnewend[i]; j++) {
          len += data->offset->length[j - data->offset->pstart[0]];
          // keep record of where each chunk is moved to
          data->offset->movedlocation[j - data->offset->pstart[0]] = data->offset->pend[Numthreads-1] + j - data->offset->pnewstart[i];
          data->offset->global[data->offset->pend[Numthreads-1] + j - data->offset->pnewstart[i] - data->offset->pstart[0]] =  data->offset->global[j - data->offset->pstart[0]];
        }
        memcpy(data->ddata + data->offset->offset[data->offset->pend[Numthreads-1] - data->offset->pstart[0]],data->ddata + data->offset->offset[data->offset->pnewstart[i] - data->offset->pstart[0]],len*(data->type == IPM_NUMERIC ? sizeof(double) : sizeof(IPMK_Index)));
        memcpy(data->offset->length + data->offset->pend[Numthreads-1]-data->offset->pstart[0],data->offset->length + data->offset->pnewstart[i]-data->offset->pstart[0],(data->offset->pnewend[i]-data->offset->pnewstart[i])*sizeof(IPMK_Index));
        for (int j=0; j<data->offset->pnewend[i] - data->offset->pnewstart[i]; j++) {
          data->offset->offset[j + data->offset->pend[Numthreads-1] - data->offset->pstart[0] + 1] = data->offset->offset[j + data->offset->pend[Numthreads-1] - data->offset->pstart[0]] + data->offset->length[j + data->offset->pend[Numthreads-1] - data->offset->pstart[0]];
        }
	
        data->offset->pend[Numthreads-1] += data->offset->pnewend[i] - data->offset->pnewstart[i];
      }
      //  Assign all owned values to first thread
      data->offset->pend[0] = data->offset->pend[Numthreads-1];
      for (int i=1; i<Numthreads; i++) {
        data->offset->pend[i] = data->offset->pstart[i] = data->offset->pend[0];
      }
      int64_t ntotal = data->offset->pend[0] - data->offset->pstart[0]; 
      MPI_Allreduce(&ntotal,&data->offset->ptotal,1,MPI_INT64_T,MPI_SUM,MPI_COMM_WORLD);
      
      // map idata to new locations
      if (data->type == IPM_INDEX) {
	int rank; MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	for (int i=0; i<data->offset->pend[0]-data->offset->pstart[0]; i++) {
	  //  printf("[%d] i %d first prefix data[2*i] %d\n",rank,2*i,(int)data->idata[2*i]);
	  //  printf("[%d] i %d first prefix data[2*i+1] %d\n",rank,2*i+1,(int)data->idata[2*i+1]);
	}
	
        for (int i=0; i<data->offset->pend[Numthreads-1] - data->offset->pstart[0]; i++) {
	  for (int j=0; j<data->offset->length[i]; j++) {
	    data->idata[data->offset->offset[i]+j] = data->dataoffset->movedlocation[data->idata[data->offset->offset[i]+j]-data->dataoffset->pstart[0]];
	  }
        }
      }
    }
  }
    
    
  if ((func->opts & IPM_FUNCTION_REPARTITION) && func->n == 2 && func->array[0]->type == IPM_NUMERIC && func->array[1]->type == IPM_INDEX) {
    /*  printf("before\n");
	IPMU_SequentialPhaseBegin(err);
	IPM_ArrayPrint1(func->array[0],err);
	IPMU_SequentialPhaseEnd(err);
	IPMU_SequentialPhaseBegin(err);
	IPM_ArrayPrint2(func->array[0],func->array[1],err);
	IPMU_SequentialPhaseEnd(err);*/
    IPMU_FunctionPartition(func,err);
    /*    printf("after\n");
	  IPMU_SequentialPhaseBegin(err);
	  IPM_ArrayPrint1(func->array[0],err);
	  IPMU_SequentialPhaseEnd(err);
	  IPMU_SequentialPhaseBegin(err);
	  IPM_ArrayPrint2(func->array[0],func->array[1],err);
	  IPMU_SequentialPhaseEnd(err);*/
  }
  //   assign different parts of the local ownership to each thread
  // test: assign everything to thread 1 instead of 0 then debug rest of code


  for (q=0; q<func->n; q++) {
    IPM_Array data = func->array[q];
    if (data->type == IPM_INDEX || data->type == IPM_NUMERIC) {
      
      //   local values among all threads
      int64_t nlocal = data->offset->pend[0] - data->offset->pstart[0];
      for (int i=0; i<Numthreads; i++) {
        data->offset->pend[i] = data->offset->pstart[i] + nlocal/Numthreads + ((nlocal % Numthreads) > i);
        if (i < Numthreads-1) data->offset->pstart[i+1] = data->offset->pend[i];
      }
      
      
      /* setup space for new points added by any threads */
      data->offset->pnewstart[0]                       = data->offset->pend[Numthreads-1];
      data->offset->pnewend[0]                         = data->offset->pnewstart[0]; 
      data->offset->offset[data->offset->pnewstart[0]-data->offset->pstart[0]] = data->offset->offset[data->offset->pend[Numthreads-1]-data->offset->pstart[0]];
      for (int i=1; i<Numthreads; i++) {
	data->offset->pnewstart[i] = data->offset->pnewstart[i-1] + IPM_THREADSPLIT;
	data->offset->pnewend[i] = data->offset->pnewstart[i];    
        data->offset->offset[data->offset->pnewstart[i]-data->offset->pstart[0]] =  data->offset->offset[data->offset->pnewstart[i-1]-data->offset->pstart[0]] + IPM_THREADSPLIT;
      }
    }
  }
  *err = 0;
}

void IPM_FunctionLaunch(IPM_Function func,IPM_Error *err)
{
  *err = 0;
  int q;
  IPM_RArray r = NULL;
  for (q=0; q<func->n; q++) {
    r = (IPM_RArray) func->array[q];
    if ((r->type == IPM_REDUNDANT) && (func->mtype[q] == IPM_MEMORY_ADD) && (func->opts | IPM_FUNCTION_KERNEL)) {
      IPMU_RArrayZero(r,err);
      break;
    } else if ((r->type == IPM_REDUNDANT) && (func->opts | IPM_FUNCTION_KERNEL)) {
      for (int i=1; i<Numthreads; i++) {
        memcpy(r->ddata[i],r->ddata[0],r->length*sizeof(double));
      }
      break;
    } else r = NULL;
  }
  for (int j=0; j<Numthreads; j++) {
    Currentthread = j;
    int launch = 0, noidata = 1,i,ptotal = 0,rank;
    IPMU_FunctionPreLaunch(func,err);
    for (i=0; i<func->n; i++) { 
      if (func->array[i]->type == IPM_REDUNDANT || func->array[i]->type == IPM_NUMERIC) continue;
      noidata = 0;
      ptotal += func->array[i]->offset->ptotal;
      if (func->array[i]->offset->pend[j] != func->array[i]->offset->pstart[j]) {launch = 1; continue;}   // if any idata has local values then run task
    }
    if (noidata) {
      for (i=0; i<func->n; i++) {        
	if (func->array[i]->type == IPM_REDUNDANT || func->array[i]->type == IPM_INDEX) continue;      
	ptotal += func->array[i]->offset->ptotal;                                                    // if there is absolutely no idata provided to task 
	if (func->array[i]->offset->pend[j] != func->array[i]->offset->pstart[j]) {launch = 1; continue;}   //   and data has local values then run task
      }
    }
    if (func->opts & IPM_FUNCTION_SEQUENTIAL) {
      IPMU_SequentialPhaseBegin(err);
    }
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    if (launch || (!rank && !ptotal && !Currentthread)) {
      if (func->n == 1) {
	void (*f)(IPM_Array,IPM_Error*) = (void (*)(IPM_Array,IPM_Error*)) func->f;
	(*f)(func->array[0],err);
      } else if (func->n == 2) {
	void (*f)(IPM_Array,IPM_Array,IPM_Error*) = (void (*)(IPM_Array,IPM_Array,IPM_Error*)) func->f;
	(*f)(func->array[0],func->array[1],err);
      } else if (func->n == 3) {
	void (*f)(IPM_Array,IPM_Array,IPM_Array,IPM_Error*) = (void (*)(IPM_Array,IPM_Array,IPM_Array,IPM_Error*)) func->f;
	(*f)(func->array[0],func->array[1],func->array[2],err);
      } else if (func->n == 4) {
	void (*f)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*) = (void (*)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*)) func->f;
	(*f)(func->array[0],func->array[1],func->array[2],func->array[3],err);
      } else if (func->n == 5) {
	void (*f)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*) = (void (*)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*)) func->f;
	(*f)(func->array[0],func->array[1],func->array[2],func->array[3],func->array[4],err);
      } else {
	IPM_SetErrorVoid(IPM_ERROR_WRONG_STATE);
      }
    }
  
    if (func->opts & IPM_FUNCTION_SEQUENTIAL) {
      IPMU_SequentialPhaseEnd(err);
    }
  }
  Currentthread = 0;
  IPMU_FunctionPostLaunch(func,err);
  if (r && (func->opts | IPM_FUNCTION_KERNEL)) {
    IPMU_RArrayReduce(r,err);
  }
}


/*
      !!!!!!!!!!!! The following routines ONLY use pstart[0] and pend[0] not the ones for any other threads
*/
/*
    Change data->global values to use a single memory layout 
*/
void IPMU_FixData(IPM_Array data,IPM_Error *err)
{
  *err = 0;
  IPM_Offset offset = data->offset;

  // plocal is number of data items owned by this process
  int plocal = (int)offset->pend[0] - offset->pstart[0];

  // procoffset[rank] is number of items owned by all previous processes
  int size,rank;
  MPI_Comm_size(MPI_COMM_WORLD,&size);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  //  proccnt and procoffset describe the parallel layout of offset->length
  int *proccnt = (int*) malloc(size*sizeof(int));
  if (!proccnt) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  MPI_Allgather(&plocal,1,MPI_INT,proccnt,1,MPI_INT,MPI_COMM_WORLD);
  int *procoffset = (int*) malloc((size+1)*sizeof(int));
  if (!procoffset) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  procoffset[0] = 0;
  int i;
  for (i=1; i<size+1; i++) {
    procoffset[i] = procoffset[i-1] + proccnt[i-1];
  }
  //  Change data->offset->global to use single memory layout 
  for (i=offset->pstart[0]; i<offset->pend[0]; i++) {
    //   printf("[%d] fixing nonghosts %d %d\n",rank,i,(int)offset->global[i-offset->pstart[0]]);
    offset->global[i - offset->pstart[0]] = procoffset[rank] + (offset->global[i - offset->pstart[0]] % IPM_SPLIT);
    //   printf("[%d] fixing nonghosts %d %d\n",rank,i,(int)offset->global[i-offset->pstart[0]]);
  }
  for (i=offset->ghoststart; i<offset->ghostend; i++) {
    //    printf("[%d] fixing ghosts %d %d\n",rank,i,(int)offset->global[i-offset->pstart[0]]);
    offset->global[i - offset->pstart[0]] = procoffset[offset->global[i-offset->pstart[0]]/IPM_SPLIT] + offset->global[i-offset->pstart[0]] % IPM_SPLIT;
    //   printf("[%d] fixed ghosts %d %d\n",rank,i,(int)offset->global[i-offset->pstart[0]]);
  }
  free(procoffset);
}


/*
   Change idata->idata values to use the single memory layout of ddata
*/
void IPMU_FixIdata(IPM_Offset offset,IPM_Array idata,IPM_Error *err)
{
  int i,j;
  for (i=idata->offset->pstart[0]; i<idata->offset->pend[0]; i++) {
    for (j=0; j<idata->offset->length[i-idata->offset->pstart[0]]; j++) {
      idata->idata[idata->offset->offset[i-idata->offset->pstart[0]]+j] = offset->global[idata->idata[idata->offset->offset[i-idata->offset->pstart[0]]+j] - offset->pstart[0]];
    }
  }
  for (i=0; i<2*(idata->offset->pend-idata->offset->pstart); i++) {
    //    printf(" fixed idata i %d %d\n",i,(int)idata->idata[i]);
  }
  *err = 0;
}

/*
   Gives each process a complete copy of an IPM_Array 
*/
void IPMU_ArrayGather(IPM_Offset offset,IPM_Arraytype indtype,void *data,int64_t *outtotal,int64_t *outtotallength,int64_t **outalllength,int64_t **outalloffset,void **outalldata,int **procoffset,IPM_Error *err)
{
  *err = 0;
  int rank,size;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&size);
  int total = offset->ptotal;
  int *recvcnts = malloc(size*sizeof(int));
  if (!recvcnts) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  int local = offset->pend[0] - offset->pstart[0];
  MPI_Allgather(&local,1,MPI_INT,recvcnts,1,MPI_INT,MPI_COMM_WORLD);
  int i,*disp = malloc(size*sizeof(int));
  if (!disp) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  disp[0] = 0; for (i=1; i<size; i++) disp[i] = disp[i-1] + recvcnts[i-1];
  int64_t *alllength = malloc(total*sizeof(int64_t));
  if (!alllength) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  MPI_Allgatherv(offset->length,local,MPI_INT64_T, alllength,recvcnts,disp,MPI_INT64_T,MPI_COMM_WORLD);
  int totallength = 0;
  for (i=0; i<total; i++) {
    totallength += alllength[i];
  }
  if (outtotallength) *outtotallength = totallength;
  int64_t *alloffset = malloc((total+1)*sizeof(int64_t));
  if (!alloffset) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  alloffset[0] = 0;
  for (i=1; i<total+1; i++) alloffset[i] = alloffset[i-1] + alllength[i-1];
  int locallength = 0;
  for (i=0; i<local; i++) {
    locallength += offset->length[i];
  }
  int64_t *alldata = malloc(totallength*sizeof(int64_t));
  if (!alldata) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  MPI_Allgather(&locallength,1,MPI_INT,recvcnts,1,MPI_INT,MPI_COMM_WORLD);
  disp[0] = 0; for (i=1; i<size; i++) disp[i] = disp[i-1] + recvcnts[i-1];
  MPI_Datatype dtype;
  if (indtype == IPM_INDEX) dtype = MPI_INT64_T;
  else if (indtype == IPM_NUMERIC) dtype = MPI_DOUBLE;
  else IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  MPI_Allgatherv(data,locallength,dtype, alldata,recvcnts,disp,dtype,MPI_COMM_WORLD);
  free(recvcnts);
  *outtotal = total;
  *outalllength = alllength;
  *outalloffset = alloffset;
  *outalldata = (void*)alldata;
  if (procoffset) {
    *procoffset = disp;
  } else {
    free(disp);
  }
}
  
/*
    Given a func with a single data and idata; partitions the idata and sets up the ghost locations for data
*/
void IPMU_FunctionPartition(IPM_Function func,IPM_Error *err)
{
  if (func->n != 2) IPM_SetErrorVoid(IPM_ERROR_WRONG_STATE);
  IPM_Array data = func->array[0];
  IPM_Array idata = func->array[1];

  // moving data around voids any previously created kernel contexts 
  for (int i=0; i<Numthreads; i++) {
    if (data->offset->kcontext[i]) {
      free(data->offset->kcontext[i]);
      data->offset->kcontext[i] = NULL;
    }
    if (idata->offset->kcontext[i]) {
      free(idata->offset->kcontext[i]);
      idata->offset->kcontext[i] = NULL;
    }
  }

  int rank,size;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&size);

  // rewrite everything using a global numbering
  IPMU_FixData(data,err);
  for (int i=0; i<idata->offset->pend[0]-idata->offset->pstart[0]; i++) {
    //  printf("[%d] i %d prefix idata[2*i] %d\n",rank,2*i,(int)idata->idata[2*i]);
    //  printf("[%d] i %d prefix idata[2*i+1] %d\n",rank,2*i+1,(int)idata->idata[2*i+1]);
  }
  IPMU_FixData(idata,err);
  for (int i=0; i<idata->offset->pend[0]-idata->offset->pstart[0]; i++) {
    //  printf("[%d] i %d prefix idata[2*i] %d\n",rank,2*i,(int)idata->idata[2*i]);
    //  printf("[%d] i %d prefix idata[2*i+1] %d\n",rank,2*i+1,(int)idata->idata[2*i+1]);
  }
  IPMU_FixIdata(data->offset,idata,err);

  for (int i=0; i<idata->offset->pend[0]-idata->offset->pstart[0]; i++) {
    //  printf("[%d] i %d post fix idata[2*i] %d\n",rank,2*i,(int)idata->idata[2*i]);
    //  printf("[%d] i %d post fix idata[2*i+1] %d\n",rank,2*i+1,(int)idata->idata[2*i+1]);
  }


  // Every process gets all the data in order to allow simple repartitioning and ghosting

  int64_t *allplength,*allidata,ptotal,*allpoffset,totallength;
  IPMU_ArrayGather(idata->offset,idata->type,idata->idata,&ptotal,&totallength,&allplength,&allpoffset,(void**)&allidata,NULL,err);

  for (int i=0; i<idata->offset->pend[0]-idata->offset->pstart[0]; i++) {
    //  printf("[%d] i %d idata[2*i] %d\n",rank,2*i,(int)idata->idata[2*i]);
    //  printf("[%d] i %d idata[2*i+1] %d\n",rank,2*i+1,(int)idata->idata[2*i+1]);
  }


  for (int i=0; i<ptotal; i++) {
    //  printf("[%d] i %d allidata[2*i] %d\n",rank,2*i,(int)allidata[2*i]);
    //  printf("[%d] i %d allidata[2*i+1] %d\n",rank,2*i+1,(int)allidata[2*i+1]);
  }

  int64_t *alllength,total,*alloffset;
  double *alldata;
  int    *procoffset;
  IPMU_ArrayGather(data->offset,data->type,data->ddata,&total,NULL,&alllength,&alloffset,(void**)&alldata,&procoffset,err);

  int j;
  int i;
  //  for (i=0; i<totallength; i++) printf("[%d] allidata %d %d\n",rank,i,(int)allidata[i]);

  //  split the idata amoung the processes; no ghost locations
  int plocal = ptotal/size + ((ptotal % size) > rank);
  int plocalstart;
  MPI_Scan(&plocal,&plocalstart,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
  plocalstart -= plocal;
  int locallen = 0;
  for (i=plocalstart; i<plocalstart+plocal; i++) {
    locallen += allplength[i];
  }

  int64_t *myidata = malloc(IPM_SPLIT*sizeof(int64_t));
  if (!myidata) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  int64_t *myplens = malloc(IPM_SPLIT*sizeof(int64_t));
  if (!myplens) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  memcpy(myplens,allplength+plocalstart,plocal*sizeof(int64_t));
  int64_t myidatastart = 0;
  for (i=0; i<plocalstart; i++) myidatastart += allplength[i];
  memcpy(myidata,allidata+myidatastart,locallen*sizeof(int64_t));

  int64_t *mypoffset = malloc(IPM_SPLIT*sizeof(int64_t));
  if (!mypoffset) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  memcpy(mypoffset,allpoffset+plocalstart,(plocal+1)*sizeof(int64_t));
  for (i=plocal; i>0; i--) {
    mypoffset[i] = mypoffset[i] - mypoffset[0];
  }
  mypoffset[0] = 0;

  IPMK_Index *mypglobal = malloc(IPM_SPLIT*sizeof(IPMK_Index));
  if (!mypglobal) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  for (i=0; i<plocal; i++) {
    mypglobal[i] = IPM_SPLIT*rank + i; 
  }


  for (i=0; i<2*plocal; i++) {
    //    printf("[%d] mydata %d\n",rank,(int)myidata[i]);
  }

  //   determine all data items I need from what is referenced in myidata
  int *mark = malloc(total*sizeof(int));
  if (!mark) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  memset(mark,0,total*sizeof(int));
  for (i=0; i<locallen; i++) {
    if (myidata[i] >= total) IPM_SetErrorVoid(IPM_ERROR_SYSTEM);
    mark[myidata[i]] = 1;
  }
  int cnt = 0;
  for (i=0; i<total; i++) if (mark[i]) cnt++;


  // do the  decomposition of data amoung the processes
  int local = total/size + ((total % size) > rank);
  int localstart;
  MPI_Scan(&local,&localstart,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
  localstart -= local;
  int *localstarts = malloc(size*sizeof(int));   // which index each processes worth of data starts on
  if (!localstarts) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  MPI_Allgather(&localstart,1,MPI_INT,localstarts,1,MPI_INT,MPI_COMM_WORLD);
  cnt += local;  // enough room for all indices needed on this process
  int64_t *mypoints = malloc(IPM_SPLIT*sizeof(int64_t));
  if (!mypoints) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  cnt = 0;
  // markcnt is where in the ghosted mydata each index is
  int *markcnt = malloc(total*sizeof(int));
  if (!markcnt) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  int64_t *myglobal = malloc(IPM_SPLIT*sizeof(int64_t));
  if (!myglobal) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  for (i=0; i<local; i++) {           // first list local index
    markcnt[localstart + i] = IPM_SPLIT*rank + i;
    myglobal[i] = IPM_SPLIT*rank + i;
    mypoints[cnt++] = localstart + i;
    mark[localstart + i] = 0;
  }
  int ghoststart = IPM_SPLIT/2;
  for (i=0; i<total; i++) {           // now list ghost indices
    if (mark[i]) {
      markcnt[i] = IPM_SPLIT*rank + ghoststart + cnt - local;
      for (j=size-1; j>=0; j--) {
        if (localstarts[j] <= i) {
          break;
        }
      }
      myglobal[ghoststart + cnt - local] = IPM_SPLIT*j + i - localstarts[j];
      mypoints[cnt++] = i;
    }
  }
  int64_t *mylens   = malloc(IPM_SPLIT*sizeof(int64_t));  
  if (!mylens) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  memset(mylens,0,IPM_SPLIT*sizeof(int64_t));

  int lcnt = 0;
  for (i=0; i<local; i++) {
    lcnt += mylens[i] = alllength[mypoints[i]];
  }
  for (i=local; i<cnt; i++) {
    lcnt += mylens[i - local + ghoststart ] = alllength[mypoints[i]];
  }

  int64_t *myoffset = malloc(IPM_SPLIT*sizeof(int64_t));
  if (!myoffset) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  memset(myoffset,0,IPM_SPLIT*sizeof(int64_t));
  myoffset[0] = 0;
  for (i=1; i<local+1; i++) myoffset[i] += myoffset[i-1] + alllength[mypoints[i-1]];
  myoffset[ghoststart] = IPM_SPLIT/2;
  for (i=1; i<cnt-local; i++) myoffset[i + ghoststart] += myoffset[i-1+ghoststart] + alllength[i-1];

  double *mydata = malloc(IPM_SPLIT*sizeof(double));
  if (!mydata) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
  for (i=0; i<local; i++) {
    memcpy(mydata+myoffset[i],alldata+alloffset[mypoints[i]],mylens[i]*sizeof(double));
  }
  for (i=local; i<cnt; i++) {
    memcpy(mydata+myoffset[i-local+ghoststart],alldata+alloffset[mypoints[i]],mylens[i-local+ghoststart]*sizeof(double));
  }

  //   record idata using per task 
  for (i=0; i<locallen; i++) {
    //    printf("[%d] i %d %d \n",rank,i,(int)myidata[i]);
    myidata[i] = markcnt[myidata[i]];
    //    printf("[%d] new  %d \n",rank,(int)myidata[i]);
  }


  free(data->ddata);
  free(data->offset->offset);
  free(data->offset->length);
  free(data->offset->global);
  data->ddata = mydata;
  data->offset->global = myglobal;
  data->offset->offset = myoffset;
  data->offset->length = mylens;
  data->offset->pstart[0] = IPM_SPLIT*rank;
  data->offset->pend[0]   = IPM_SPLIT*rank + local;
  data->offset->pnewstart[0] = data->offset->pend[0];
  data->offset->pnewend[0]   = data->offset->pnewstart[0];
  data->offset->ghoststart = data->offset->pstart[0] + ghoststart;
  data->offset->ghostend = data->offset->ghoststart + cnt - local;


  free(idata->idata);
  free(idata->offset->offset);
  free(idata->offset->length);
  free(idata->offset->global);
  idata->idata = myidata;
  idata->offset->global = mypglobal;
  idata->offset->offset = mypoffset;
  idata->offset->length = myplens;
  idata->offset->pstart[0] = IPM_SPLIT*rank;
  idata->offset->pend[0]   = IPM_SPLIT*rank + plocal;
  idata->offset->pnewstart[0] = idata->offset->pend[0];
  idata->offset->pnewend[0]   = idata->offset->pnewstart[0];
}

void IPM_Launch(IPM_Function func,IPM_Array data, ...)
{
  va_list  Argp;
  func->array[0] = data;
  va_start(Argp,data);
  for (int i=1; i<func->n; i++) {
    func->array[i] = (IPM_Array) va_arg(Argp, IPM_Array);
  }
  IPM_Error *err = (IPM_Error*) va_arg(Argp, IPM_Error*);;
  va_end(Argp);
  IPM_FunctionLaunch(func,err);
}

IPM_Function IPM_FunctionCreate(IPM_FunctionOption opts,void *f,int64_t n,...)
{
  IPM_Function func = malloc(sizeof(struct _p_IPM_Function));
  // no good way to handle failed malloc
  func->opts = opts;
  func->f    = (IPM_Error (*)(IPM_Array,...)) f;
  func->n    = n;
  va_list  Argp;
  va_start(Argp,n);
  int i = 0;
  for (i=0; i<n; i++) {
    func->mtype[i] = (IPM_Memorytype) va_arg(Argp, IPM_Memorytype);
  }
  IPM_Error *err = (IPM_Error*) va_arg(Argp, IPM_Error*);
  va_end(Argp);
  *err = 0;
  return func;
}


