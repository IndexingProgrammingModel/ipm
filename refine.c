/*
 Copyright (c) 2013 UChicago Argonne, LLC

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <ipm.h>
#include <cairo-pdf.h>
cairo_t *cr;

/*
    This code demonstrates the use of the IPM parallel programming model with finite element mesh refinement 
   for triangular elements.

   Refines a circle.

    See ipm.h and IPM.pdf for more details on the model.


*/

// Draws edges, ignores elements completely
void kDraw(IPMK_ArrayNumeric u,IPMK_ArrayIndex edges,IPMK_ArrayIndex elements,IPMK_Error *err)
{
  if (edges->dataoffset != u->offset) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);
  for (IPMK_Index p=IPMK_ArrayIndexStart(edges,err); p<IPMK_ArrayIndexEnd(edges,err); p++) {
    const IPMK_Index *ps = IPMK_ArrayIndexGet(edges,p,err);

    cairo_move_to(cr,IPMK_ArrayNumericGet(u,ps[0],err)[0],IPMK_ArrayNumericGet(u,ps[0],err)[1]); 
    cairo_line_to(cr,IPMK_ArrayNumericGet(u,ps[1],err)[0],IPMK_ArrayNumericGet(u,ps[1],err)[1]); 
    cairo_close_path(cr);
    cairo_stroke (cr);
  }
}

/*
    This kernel/task is called on a completely empty set of IPM objects and puts two double precision entries in the u object
  and a single "element", consisting of the "addresses" (called indices) of those two entries, into the e object.
*/
void kCreate(IPMK_ArrayNumeric u,IPMK_ArrayIndex edges,IPMK_ArrayIndex elements,IPMK_Error *err)
{
  if (u->offset->ptotal > 0) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);
  if (edges->offset->ptotal > 0) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);
  if (elements->offset->ptotal > 0) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);

  double     xy[5][2]        = {{.5,0},{1,.5},{.5,1},{0,.5},{.5,.5}};
  int        edgeid[8][2]    = {{0,1},{1,4},{4,0},{1,2},{2,4},{2,3},{3,4},{3,0}};
  int        elementid[4][3] = {{0,1,2},{1,3,4},{4,5,6},{6,7,2}};

  /*  add vertices to data */
  IPMK_Index vertexindices[5];
  for (int i=0; i<5; i++) vertexindices[i] = IPMK_ArrayNumericAdd(u,2,xy[i],err);

  /*  add edges to data */
  IPMK_Index edgeindices[8];
  for (int i=0; i<8; i++) {
    IPMK_Index ps[2] = {vertexindices[edgeid[i][0]],vertexindices[edgeid[i][1]]};
    edgeindices[i] = IPMK_ArrayIndexAdd(edges,2,ps,err);
  }

  /* add elements to data */
  for (int i=0; i<4; i++) {
    IPMK_Index ps[3] = {edgeindices[elementid[i][0]],edgeindices[elementid[i][1]],edgeindices[elementid[i][2]]};
    IPMK_ArrayIndexAdd(elements,3,ps,err);
  }
}


/*
   This kernel/task splits each element it is provided into three elements by introducing a new double precision entry at its mid-point
*/
void kRefine(IPMK_ArrayNumeric u,IPMK_ArrayIndex edges,IPMK_ArrayIndex elements,IPMK_ArrayIndex edgechildren, IPMK_ArrayIndex elementchildren,IPMK_Error *err)
{
  if (edges->dataoffset != u->offset) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);

  /* refine the edges */
  for (IPMK_Index p=IPMK_ArrayIndexStart(edges,err); p<IPMK_ArrayIndexEnd(edges,err); p++) {
    const IPMK_Index *vertices = IPMK_ArrayIndexGet(edges,p,err); // vertices of elements
    const double    *xy0 = IPMK_ArrayNumericGet(u,vertices[0],err); // coors of vertex
    const double    *xy1 = IPMK_ArrayNumericGet(u,vertices[1],err);

    // add new vertex into vertex array
    double     xynew[2] = {(xy0[0]+xy1[0])/2.0,(xy0[1]+xy1[1])/2.0};
    IPMK_Index newindex = IPMK_ArrayNumericAdd(u,2,xynew,err);

    // add two new edges
    IPMK_Index indices[2];
    indices[0] = vertices[0];
    indices[1] = newindex;
    IPMK_Index child[2];
    child[0] = IPMK_ArrayIndexAdd(edges,2,indices,err);
    indices[0] = newindex;
    indices[1] = vertices[1];
    child[1] = IPMK_ArrayIndexAdd(edges,2,indices,err);
    IPMK_ArrayIndexAdd(edgechildren,2,child,err);
  }

  /* refine the elements */
  for (IPMK_Index p=IPMK_ArrayIndexStart(elements,err); p<IPMK_ArrayIndexEnd(elements,err); p++) {
    const IPMK_Index *vertices = IPMK_ArrayIndexGet(elements,p,err); // vertices of elements
    const IPMK_Index *e  = IPMK_ArrayIndexGet(edges,vertices[0],err); // coors of vertex
    const double    *xy0 = IPMK_ArrayNumericGet(u,vertices[0],err); // coors of vertex
    const double    *xy1 = IPMK_ArrayNumericGet(u,vertices[1],err);

    // add new vertex into vertex array
    double     xynew[2] = {(xy0[0]+xy1[0])/2.0,(xy0[1]+xy1[1])/2.0};
    IPMK_Index newindex = IPMK_ArrayNumericAdd(u,2,xynew,err);

    // add two new edges
    IPMK_Index indices[2];
    indices[0] = vertices[0];
    indices[1] = newindex;
    IPMK_Index child[2];
    child[0] = IPMK_ArrayIndexAdd(edges,2,indices,err);
    indices[0] = newindex;
    indices[1] = vertices[1];
    child[1] = IPMK_ArrayIndexAdd(edges,2,indices,err);
    IPMK_ArrayIndexAdd(edgechildren,2,child,err);
  }

}

#include <mpi.h>
int main(int argc,char **args)
{
  IPM_Error      err;
  char           filename[128];

  IPM_Initialize(&argc,&args,&err);
  int            rank;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  snprintf(filename,128,"pdffile_%d.pdf",rank);
  cairo_surface_t *surface = cairo_pdf_surface_create(filename,400,400);
  cr = cairo_create (surface);
  cairo_scale (cr, 400, 400);
  cairo_set_line_width (cr, 1.0/200.);
  cairo_set_source_rgb (cr, 0, 0, 0);


  
  
  IPM_ArrayNumeric u = IPM_ArrayNumericCreate(&err);
  IPM_ArrayIndex edges = IPM_ArrayIndexCreate(&err);
  IPM_ArrayIndexSetOffset(edges,IPM_ArrayNumericGetOffset(u,&err),&err);
  IPM_ArrayIndex elements = IPM_ArrayIndexCreate(&err);
  IPM_ArrayIndexSetOffset(elements,IPM_ArrayIndexGetOffset(edges,&err),&err);
  
  IPM_Function myCreate = IPM_FunctionCreate(IPM_FUNCTION_REPARTITION,kCreate,2,IPM_MEMORY_WRITE,IPM_MEMORY_WRITE,&err);
  IPM_Launch(myCreate,(IPM_Array)u,edges,elements,&err);

  IPM_ArrayIndex edgechildren = IPM_ArrayIndexCreate(&err);
  IPM_ArrayIndexSetOffset(edgechildren,IPM_ArrayIndexGetOffset(edges,&err),&err);
  IPM_ArrayIndex elementchildren = IPM_ArrayIndexCreate(&err);
  IPM_ArrayIndexSetOffset(elementchildren,IPM_ArrayIndexGetOffset(elements,&err),&err);


  IPM_Launch(ipm_arrayprint1,(IPM_Array)u,&err);
  IPM_Launch(ipm_arrayprint2,(IPM_Array)u,edges,&err);

  IPM_Function myDraw = IPM_FunctionCreate(IPM_FUNCTION_REPARTITION,kDraw,3,IPM_MEMORY_READ,IPM_MEMORY_READ,IPM_MEMORY_READ,&err);
  IPM_Launch(myDraw,(IPM_Array)u,edges,elements,&err);


  IPM_Function myRefine = IPM_FunctionCreate(IPM_FUNCTION_REPARTITION,kRefine,5,IPM_MEMORY_WRITE,IPM_MEMORY_WRITE,IPM_MEMORY_WRITE,IPM_MEMORY_WRITE,IPM_MEMORY_WRITE,&err);
  IPM_Launch(myRefine,(IPM_Array)u,edges,elements,edgechildren,elementchildren,&err);
  IPM_Launch(myDraw,(IPM_Array)u,edges,elements,&err);

#if defined(foo)
  IPM_Launch(ipm_arrayprint1,(IPM_Array)u,&err);
  IPM_Launch(ipm_arrayprint2,(IPM_Array)u,edges,&err);

  IPM_Launch(myRefine,(IPM_Array)u,edges,elements,&err);

  IPM_Launch(ipm_arrayprint1,(IPM_Array)u,&err);
  IPM_Launch(ipm_arrayprint2,(IPM_Array)u,edges,&err);
#endif

  IPM_Finalize(&err);
  cairo_destroy(cr);
  cairo_surface_destroy (surface);
  return 0;
}
