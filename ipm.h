/*
 Copyright (c) 2013 UChicago Argonne, LLC

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/
#if !defined(__IPM_H)
#define __IPM_H
#include <stdint.h>

/*
   See the appendix of IPM.pdf for a concise summary of the API.

*/

typedef int                        IPM_Error;
typedef int64_t                    IPMK_Index;
typedef struct _p_IPM_Offset       *IPM_Offset;
typedef struct _p_IPM_Array        *IPM_Array;
typedef struct _p_IPM_ArrayNumeric *IPM_ArrayNumeric;
typedef struct _p_IPM_ArrayIndex   *IPM_ArrayIndex;
typedef struct _p_IPM_RArray       *IPM_RArray;
typedef struct _p_IPM_Function     *IPM_Function;
typedef enum {IPM_MEMORY_READ, IPM_MEMORY_WRITE, IPM_MEMORY_ADD} IPM_Memorytype;
typedef enum {IPM_INDEX, IPM_NUMERIC, IPM_REDUNDANT} IPM_Arraytype;

#define IPMK_Error        IPM_Error
#define IPMK_Offset       IPM_Offset 
#define IPMK_ArrayNumeric IPM_ArrayNumeric
#define IPMK_ArrayIndex   IPM_ArrayIndex
#define IPMK_RArray       IPM_RArray

void IPM_Initialize(int*,char***,IPM_Error*);
void IPM_Finalize(IPM_Error*);
void IPM_Printf(const char[],...);

extern int Numthreads;
extern int Currentthread;
#define MAXTHREAD 4

struct _p_IPM_Offset {
  int64_t    *offset;               // Indicate the starting location of each "point-worth" of data in IPM_Array
  int64_t    *length;               // How much data is at each "point-worth" of data
  IPMK_Index *global;               // global number of each point including ghosts
  IPMK_Index  pstart[MAXTHREAD],pend[MAXTHREAD];           // indices of any locally owned data 
  IPMK_Index  ghoststart,ghostend;   // range of any locally contained ghost values
  void       *context;

  // private part of structure 
  IPMK_Index pmax;
  IPMK_Index pnewstart[MAXTHREAD],pnewend[MAXTHREAD];
  IPMK_Index *movedlocation;           // when new ddata and idata are compressed to after pend[] this marks the new location for each current location
  IPMK_Index ptotal;  // total number of unique indices accross all processes/threads
  void*     kcontext[MAXTHREAD];
};

struct _p_IPM_ArrayNumeric {
  IPM_Arraytype type;
  double       *ddata;
  IPM_Offset    offset;

  // private part of structure 
  int64_t      lenmax;
  IPM_Offset   dataoffset;    // each Idata is associated with an offset of some IPM_Array object that its indices are in reference to
};

struct _p_IPM_ArrayIndex {
  IPM_Arraytype type;
  IPMK_Index    *idata;
  IPM_Offset   offset;

  // private part of structure 
  int64_t      lenmax;
  IPM_Offset   dataoffset;    // each Idata is associated with an offset of some IPM_Array object that its indices are in reference to
};

// This is the abstract base class of IPM_ArrayNumeric and IPM_ArrayIndex; users never instantiate an object of this type
struct _p_IPM_Array {
  IPM_Arraytype type;
  union {
    double         *ddata;
    IPMK_Index      *idata;
  };
  IPM_Offset   offset;

  // private part of structure 
  int64_t      lenmax;
  IPM_Offset   dataoffset;    // each Idata is associated with an offset of some IPM_Array object that its indices are in reference to
};

#define IPM_ERROR_NONE               0
#define IPM_ERROR_OUT_OF_MEMORY      1
#define IPM_ERROR_WRONG_DATATYPE     2
#define IPM_ERROR_INDEX_OUT_OF_RANGE 3
#define IPM_ERROR_WRONG_STATE        4
#define IPM_ERROR_SYSTEM             5
#define IPM_ERROR_OUT_OF_SPACE       6

#define IPMK_ERROR_NONE               IPM_ERROR_NONE
#define IPMK_ERROR_WRONG_DATATYPE     IPM_ERROR_WRONG_DATATYPE     
#define IPMK_ERROR_INDEX_OUT_OF_RANGE IPM_ERROR_INDEX_OUT_OF_RANGE 
#define IPMK_ERROR_WRONG_STATE        IPM_ERROR_WRONG_STATE        


extern int IPMU_AbortOnError;
#define IPM_SetError(flg,rtn) {*err = flg;printf("Error value %d line %d\n",*err,__LINE__);if (IPMU_AbortOnError) abort();return rtn;}
#define IPMK_SetError IPM_SetError
#define IPM_SetErrorVoid(flg) {*err = flg;printf("Error value %d line %d\n",*err,__LINE__);if (IPMU_AbortOnError) abort();return;}
#define IPMK_SetErrorVoid IPM_SetErrorVoid

static inline void IPM_OffsetCreateContext(IPM_Offset off,size_t size,const char name[],IPM_Error *err)
{
  // name is currently ignored for contexts
  if (off->context) free(off->context);
  off->context = malloc(size);
  if (!off->context) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
}

static inline void* IPM_OffsetGetContext(IPM_Offset off,const char name[],IPM_Error *err)
{
  return off->context;
}
#define IPMK_OffsetGetContext IPM_OffsetGetContext

static inline void IPMK_OffsetCreatePerKernelContext(IPM_Offset off,size_t size,const char name[],IPM_Error *err)
{
  if (off->kcontext[Currentthread]) free(off->kcontext);
  off->kcontext[Currentthread] = malloc(size);
  if (!off->kcontext[Currentthread]) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
}

static inline void* IPMK_OffsetGetPerKernelContext(IPM_Offset off,const char name[],IPM_Error *err)
{
  return off->kcontext[Currentthread];
}

static inline IPM_Offset IPM_ArrayNumericGetOffset(IPM_ArrayNumeric data,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return data->offset;
}
#define IPMK_ArrayNumericGetOffset IPM_ArrayNumericGetOffset

static inline IPM_Offset IPM_ArrayIndexGetOffset(IPM_ArrayIndex data,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return data->offset;
}
#define IPMK_ArrayIndexGetOffset IPM_ArrayIndexGetOffset

static inline IPM_Offset IPM_ArrayNumericGetDataOffset(IPM_ArrayNumeric data,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return data->dataoffset;
}
#define IPMK_ArrayNumericGetDataOffset IPM_ArrayNumericGetDataOffset

static inline IPM_Offset IPM_ArrayIndexGetDataOffset(IPM_ArrayIndex data,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return data->dataoffset;
}
#define IPMK_ArrayIndexGetDataOffset IPM_ArrayIndexGetDataOffset

static inline void IPM_ArrayIndexSetOffset(IPM_ArrayIndex data,IPM_Offset offset,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  if (data->dataoffset) {*err = IPM_ERROR_WRONG_STATE; return;}
  data->dataoffset = offset;
}

static inline IPMK_Index* IPMK_ArrayIndexGet(IPM_ArrayIndex data,IPMK_Index p,IPM_Error *err) 
{
  *err = IPM_ERROR_NONE;
  // check if p is outside of local plus ghosted range allowed
  /*  if (p < data->offset->pstart) {*err = IPM_ERROR_INDEX_OUT_OF_RANGE; return NULL;}
  if (p > data->offset->pend + data->offset->pnew - 1) {
    if (p < data->offset->ghoststart) {*err = IPM_ERROR_INDEX_OUT_OF_RANGE;return NULL;}
    if (p > data->offset->ghostend - 1) {*err = IPM_ERROR_INDEX_OUT_OF_RANGE;return NULL;}
    }*/
  return data->idata + data->offset->offset[p-data->offset->pstart[0]];
}

static inline double* IPMK_ArrayNumericGet(IPM_ArrayNumeric data,IPMK_Index p,IPM_Error *err) 
{
  *err = IPM_ERROR_NONE;
  /*if (p < data->offset->pstart) {*err = IPM_ERROR_INDEX_OUT_OF_RANGE; return NULL;}
  if (p > data->offset->pend + data->offset->pnew - 1) {
    if (p < data->offset->ghoststart) {*err = IPM_ERROR_INDEX_OUT_OF_RANGE; return NULL;}
    if (p > data->offset->ghostend - 1) {*err = IPM_ERROR_INDEX_OUT_OF_RANGE;return NULL;}
    }*/
  return data->ddata + data->offset->offset[p-data->offset->pstart[0]];
}

static inline int64_t IPMK_ArrayNumericGetLength(IPM_ArrayNumeric data,IPMK_Index p,IPM_Error *err) 
{
  *err = IPM_ERROR_NONE;
  /* if (p < data->offset->pstart) {*err = IPM_ERROR_INDEX_OUT_OF_RANGE; return 0;}
  if (p > data->offset->pend + data->offset->pnew - 1) {
    if (p < data->offset->ghoststart) {*err = IPM_ERROR_INDEX_OUT_OF_RANGE; return 0;}
    if (p > data->offset->ghostend - 1) {*err = IPM_ERROR_INDEX_OUT_OF_RANGE; return 0;}
    }*/
  return data->offset->length[p-data->offset->pstart[0]];
}

static inline int64_t IPMK_ArrayIndexGetLength(IPM_ArrayIndex data,IPMK_Index p,IPM_Error *err) 
{
  *err = IPM_ERROR_NONE;
  /* if (p < data->offset->pstart) {*err = IPM_ERROR_INDEX_OUT_OF_RANGE; return 0;}
  if (p > data->offset->pend + data->offset->pnew - 1) {
    if (p < data->offset->ghoststart) {*err = IPM_ERROR_INDEX_OUT_OF_RANGE; return 0;}
    if (p > data->offset->ghostend - 1) {*err = IPM_ERROR_INDEX_OUT_OF_RANGE; return 0;}
    }*/
  return data->offset->length[p-data->offset->pstart[0]];
}

static inline int64_t IPMK_ArrayNumericStart(IPM_ArrayNumeric data,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return data->offset->pstart[Currentthread];
}

static inline int64_t IPMK_ArrayIndexStart(IPM_ArrayIndex data,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return data->offset->pstart[Currentthread];
}

static inline int64_t IPMK_ArrayNumericEnd(IPM_ArrayNumeric data,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return data->offset->pend[Currentthread];
}

static inline int64_t IPMK_ArrayIndexEnd(IPM_ArrayIndex data,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return data->offset->pend[Currentthread];
}

static inline int64_t IPMK_ArrayNumericGhostStart(IPM_ArrayNumeric data,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return data->offset->ghoststart;
}

static inline int64_t IPMK_ArrayIndexGhostStart(IPM_ArrayIndex data,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return data->offset->ghoststart;
}

static inline int64_t IPMK_ArrayNumericGhostEnd(IPM_ArrayNumeric data,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return data->offset->ghostend;
}

static inline int64_t IPMK_ArrayIndexGhostEnd(IPM_ArrayIndex data,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return data->offset->ghostend;
}

IPM_ArrayNumeric IPM_ArrayNumericCreate(IPM_Error*);
IPM_ArrayIndex   IPM_ArrayIndexCreate(IPM_Error*);
IPMK_Index        IPMK_ArrayIndexAdd(IPM_ArrayIndex,int64_t,IPMK_Index*,IPM_Error*);
void             IPMK_ArrayIndexChange(IPM_ArrayIndex,IPMK_Index,int64_t,IPMK_Index*,IPM_Error*);
IPMK_Index        IPMK_ArrayNumericAdd(IPM_ArrayNumeric,int64_t,double*,IPM_Error*);
IPM_ArrayNumeric IPM_ArrayNumericClone(IPM_ArrayNumeric,IPM_Error*);
IPM_ArrayIndex   IPM_ArrayIndexClone(IPM_ArrayIndex,IPM_Error*);
void             IPM_ArrayNumericCopy(IPM_ArrayNumeric,IPM_ArrayNumeric,IPM_Error*);
void             IPM_ArrayIndexCopy(IPM_ArrayIndex,IPM_ArrayIndex,IPM_Error*);
void             IPM_ArrayNumericDestroy(IPM_ArrayNumeric*,IPM_Error*);
void             IPM_ArrayIndexDestroy(IPM_ArrayIndex*,IPM_Error*);

struct _p_IPM_RArray {
  IPM_Arraytype type;
  double        *ddata[MAXTHREAD];      // really only need copy per thread when it is accumulated into but this makes it easy
  int64_t       length;
  void          *context;
};

IPM_RArray IPM_RArrayCreate(int64_t,IPM_Error *err);
void       IPM_RArrayDestroy(IPM_RArray*,IPM_Error *err);
static inline double* IPM_RArrayGet(IPM_RArray data,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return data->ddata[0];
}

static inline double* IPMK_RArrayGet(IPM_RArray data,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return data->ddata[Currentthread];
}

static inline void IPM_RArrayCreateContext(IPM_RArray off,size_t size,const char name[],IPM_Error *err)
{
  if (off->context) free(off->context);
  off->context = malloc(size);
  if (!off->context) IPM_SetErrorVoid(IPM_ERROR_OUT_OF_MEMORY);
}

static inline void* IPM_RArrayGetContext(IPM_RArray off,const char name[],IPM_Error *err)
{
  return off->context;
}
#define IPMK_RArrayGetContext IPM_RArrayGetContext

typedef enum {IPM_FUNCTION_DEFAULT = 0,IPM_FUNCTION_KERNEL = 1, IPM_FUNCTION_SEQUENTIAL = 2, IPM_FUNCTION_REPARTITION = 4} IPM_FunctionOption;

struct _p_IPM_Function {
  int64_t          n;                     // number of arguments
  IPM_Memorytype   mtype[20];  
  IPM_Array        array[20];
  IPM_FunctionOption opts;
  IPM_Error        (*f)(IPM_Array,...);
};

IPM_Function IPM_FunctionCreate(IPM_FunctionOption,void * /*IPM_Error (*)(IPM_Array,...)*/,int64_t,...);
void IPM_Launch(IPM_Function,IPM_Array, ...);
static inline void IPM_LaunchWait(IPM_Function func,IPM_Error *err) {*err = IPM_ERROR_NONE;return;}

extern IPM_Function ipm_arrayprint1,ipm_arrayprint2;


#endif

