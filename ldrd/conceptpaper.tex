\documentclass[11pt]{article}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{multicol}
\usepackage{geometry} % to change the page dimensions
\usepackage{graphicx} % support the \includegraphics command and options
\geometry{letterpaper} % or letterpaper (US) or a5paper or....
\geometry{margin=1in} % for example, change the margins to 2 inches all round
\usepackage{lscape}

% Figures within a column...
\makeatletter
\newenvironment{tablehere}
{\def\@captype{table}}
{}
\newenvironment{figurehere}
{\def\@captype{figure}}
{}
\makeatother

\newenvironment{titemize} % Same as itemize, but with minimal vspace
        {\begin{list}{\labelitemi}{
                \setlength{\topsep}{0pt}
                \setlength{\parskip}{0pt}
                \setlength{\itemsep}{0pt}
                \setlength{\parsep}{0pt}
                \setlength{\leftmargin}{23pt}
                \setlength{\labelwidth}{23pt}
        }}
        {\end{list}}


\begin{document}

\title{Implementing a New Extreme Scale Parallel Programming Model with a Full Sample Application\textsuperscript{1}}


\author{
  PI: Barry Smith\textsuperscript{2} \\
  Mathematics and Computer Science Division \\
  bsmith@mcs.anl.gov.
}

\maketitle

In \cite{ipm} I proposed a new programming model called the Indexing
Programming Model (IPM) to replace the message-passing-plus-threads
(MPI+OpenMP) programming model currently used by virtually all highly
scalable simulations. In this LDRD I propose the following two concurrent tasks:
\begin{titemize} 
\item Development of a high-quality prototype implementation of an IPM
  runtime\textsuperscript{5} by a full-time postdoc or programmer\textsuperscript{7} with strong experience
  in C programming, MPI, and Pthreads.
\item Development of a fully functional time-dependent adaptive
  mesh refinement code, from scratch, for a nontrivial
  simulation\textsuperscript{5}, for example, combustion with moving
  meshes, to be implemented by twenty-five percent of my
  time\textsuperscript{7} to validate both the IPM model and the
  prototype runtime. Since it is alleged that IPM can greatly simplify
  scalable programming over MPI+OpenMP it is crucial that a
  far-from-toy simulation be produced with limited resources to
  demonstrate this point.
\end{titemize}
If these two tasks are successful after roughly one
year,\textsuperscript{6} I would then reach out to several HPC library
development groups, including,  VisIt, PETSc, SuperLU, and
Chombo, to attempt to re-implement portions of these important libraries
in IPM\textsuperscript{5} in order to demonstrate its versatility and power\textsuperscript{6}. If any of these stages fails then either
IPM must be discarded or revised to eliminate its
deficiencies.

 Computing needs initially will be modest, with
development in the first few months on laptops, then some testing and
refinement on LCRC-scale machines with final testing and refinement on
ALCF-scale machines toward the end of the first year\textsuperscript{8}. By
far the most difficult resource to locate will the postdoc/programmer
with a suitable level of experience and technical skill; if one cannot be
located, then it would be better to delay the project than proceed with
inadequate programmer resources.

The push for ever-increasing levels of concurrency and the increasing
complexity of simulation codes require a new parallel programming
model that both simplifies the development of extreme-scale codes and
makes them more efficient and scalable on newly emerging architectures
as well as on GPUs and conventional computing systems. For the past
twenty years careful attention to data locality has been crucial to
obtaining high performance. The message-passing programming model
succeeded in dominating the market because it provided a good, but not
perfect, approach for insuring data locality. With exascale systems,
data locality will remain crucial; but now resiliency, machine
jitter issues, and much more adaptive, heterogeneous simulations
require much more flexible dynamic placement of data than can not easily be done
with the message-passing model. This can handled only by having a more
powerful runtime system that determines both the size and the placement of
tasks and ensures that the data is where it needs to be when it is
needed.\textsuperscript{3}

 To achieve this, however, the runtime must be in control of the
 (generally unstructured) data, and the user must be able to indicate
 to the runtime the relationships between the data items so that the
 runtime may manipulate, partition, and distribute it for efficient
 tasks.  The IPM provides a concise API
 for the user to dynamically indicate the data arrays in the
 computations and indices into the arrays to indicate the meaning of
 the array entries (for the scientific programmer) as well as which
 entries in the arrays are needed for each particular task. The arrays
 and indices are abstract objects that may be manipulated,
 partitioned, and distributed by the runtime system as the computation
 proceeds. New indices are added as new data is generated, for
 example, with mesh refinement. No other programming model
 allows the programmer to work with completely
 conventionally programming languages but at the same time leaves the
 data in the hands of a powerful runtime system that can manage both
 data locality and the movement of data as needed for resiliency and
 load balancing \textsuperscript{4}. The API trivially supports a
 mixture of application and library code in C, Fortran, C++, CUDA, and
 OpenCL. Full discussion of the API as well as several examples of
 usage are given in \cite{ipm}.

The institution that plays the largest role in the design and (open
source) implementation of the next ``standard'' HPC programming model
has an enormous advantage in funding\textsuperscript{9} and
collaborative opportunities, plus a large amount of good will and
community impact. The opportunities for funding range from computer
science for implementation of the runtime and enhancements to the
model, mathematics for implementation of mathematical libraries that
use the model, and applications for their implementation using the
model.  For example, consider the relatively small amounts of funds
spent at Argonne on MPI/MPICH in the first couple of years to the
amount of external funding received by Argonne for MPI/MPICH related
activities over the past twenty years; this could easily be a factor
of fifty.\textsuperscript{10}

\bibliographystyle{siam}
\bibliography{/Users/barrysmith/Src/petsc/src/docs/tex/petscapp,/Users/barrysmith/Src/petsc/src/docs/tex/petsc}

\section*{Appendix: Meaning of Superscripts from the CELS Website}

\begin{enumerate} 
\itemsep-.3em
\item Title of the project
\item Proposed PI and co-PIs and associated divisions
\item One paragraph summary of the concept
\item Description of the approach to the problem and how this approach is novel
\item Description of the outcome or targeted deliverables
\item Timeline or schedule
\item Rough order of magnitude estimate of resources required – please note degree to which students, postdocs will be used.
\item Facilities or other capabilities at Argonne needed to do the work
\item Description of the approach to secure external funding once the LDRD project is complete
\item Estimate of return on investment: potential future funding vs. cost of LDRD
\end{enumerate} 

\end{document}
