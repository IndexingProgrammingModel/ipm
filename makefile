
CFLAGS = -g -I. -std=c99 -I/usr/local/include/cairo
CC     = /Users/barrysmith/Src/petsc/arch-clang/bin/mpicc

main: main.o ipm.o ipm.h
	${CC} -o main ${CFLAGS} main.o ipm.o
	/usr/bin/dsymutil main

etags:
	etags *.[ch] *.tex

moreinvolved: moreinvolved.o ipm.o ipm.h
	${CC} -o moreinvolved ${CFLAGS} moreinvolved.o ipm.o -L/usr/local/lib  -lcairo
	/usr/bin/dsymutil moreinvolved

refine: refine.o ipm.o ipm.h
	${CC} -o refine ${CFLAGS} refine.o ipm.o -L/usr/local/lib  -lcairo
	/usr/bin/dsymutil refine

clean:
	rm -f *.o main moreinvolved refine ipm.aux ipm.bbl ipm.blg ipm.log 

ipm.pdf: ipm.tex
	pdflatex ipm
	bibtex ipm
	pdflatex ipm
	pdflatex ipm

ldrd.pdf: ldrd.tex
	pdflatex ldrd
	bibtex ldrd
	pdflatex ldrd
	pdflatex ldrd
