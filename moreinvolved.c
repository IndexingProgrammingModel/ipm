/*
 Copyright (c) 2013 UChicago Argonne, LLC

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <ipm.h>
#include <cairo-pdf.h>
cairo_t *cr;

/*
    This code demonstrates the use of the IPM parallel programming model with some simple examples.

    See ipm.h and IPM.pdf for more details on the model.

    The subroutines below that begin with k are user provided "kernel" or "task" functions. They work on a piece of data provided
  by IPM and have no parallelism within them. 

    The "main" user program creates empty IPM objects (such as IPM_ArrayNumeric) and then "launches" IPM to run the kernel/task functions on 
  the data structure. The kernel/task functions add data to the IPM objects which then "grow", later calls to the kernel/task functions
  may use more processes/threads then earlier because the data structures are larger.

*/

/*
    This kernel/task is called on a completely empty set of IPM objects and puts two double precision entries in the u object
  and a single "element", consisting of the "addresses" (called indices) of those two entries, into the e object.
*/
void kCreate(IPMK_ArrayNumeric u,IPMK_ArrayIndex e,IPMK_Error *err)
{
  if (u->offset->ptotal > 0) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);
  if (e->offset->ptotal > 0) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);

  double     xy[5][2] = {{.5,0},{1,.5},{.5,1},{0,.5},{.5,.5}};
  int        id[4][3] = {{0,1,4},{1,2,4},{2,3,4},{3,0,4}};

  IPMK_Index indices[5];
  for (int i=0; i<5; i++) indices[i] = IPMK_ArrayNumericAdd(u,2,xy[i],err);
  for (int i=0; i<4; i++) {
    IPMK_Index ps[3] = {indices[id[i][0]],indices[id[i][1]],indices[id[i][2]]};
    IPMK_ArrayIndexAdd(e,3,ps,err);
  }
}

void kDraw(IPMK_ArrayNumeric u,IPMK_ArrayIndex e,IPMK_Error *err)
{
  if (e->dataoffset != u->offset) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);
  for (IPMK_Index p=IPMK_ArrayIndexStart(e,err); p<IPMK_ArrayIndexEnd(e,err); p++) {
    const IPMK_Index *ps = IPMK_ArrayIndexGet(e,p,err); // vertices of elements

    cairo_move_to(cr,IPMK_ArrayNumericGet(u,ps[0],err)[0],IPMK_ArrayNumericGet(u,ps[0],err)[1]); 
    cairo_line_to(cr,IPMK_ArrayNumericGet(u,ps[1],err)[0],IPMK_ArrayNumericGet(u,ps[1],err)[1]); 
    cairo_line_to(cr,IPMK_ArrayNumericGet(u,ps[2],err)[0],IPMK_ArrayNumericGet(u,ps[2],err)[1]); 
    //    cairo_line_to(cr,IPMK_ArrayGetDoubles(u,ps[0],err)[0],IPMK_ArrayGetDoubles(u,ps[0],err)[1]); 
    cairo_close_path(cr);
    cairo_stroke (cr);
  }
}

/*
   This kernel/task splits each element it is provided into three elements by introducing a new double precision entry at its mid-point
*/
void kRefine(IPMK_ArrayNumeric u,IPMK_ArrayIndex e,IPMK_Error *err)
{
  if (e->dataoffset != u->offset) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);

  kDraw(u,e,err);

  for (IPMK_Index p=IPMK_ArrayIndexStart(e,err); p<IPMK_ArrayIndexEnd(e,err); p++) {
    const IPMK_Index *ps = IPMK_ArrayIndexGet(e,p,err); // vertices of elements
    const double    *xy0 = IPMK_ArrayNumericGet(u,ps[0],err); // coors of vertex
    const double    *xy1 = IPMK_ArrayNumericGet(u,ps[1],err);
    const double    *xy2 = IPMK_ArrayNumericGet(u,ps[2],err);

    // add new vertex into vertex array
    double     xynew[2] = {(xy0[0]+xy1[0]+xy2[0])/3.0,(xy0[1]+xy1[1]+xy2[1])/3.0};
    IPMK_Index newindex = IPMK_ArrayNumericAdd(u,2,xynew,err);


    // add two new triangles    
    IPMK_Index indices[3];
    indices[0] = newindex;
    indices[1] = ps[1];
    indices[2] = ps[2];
    IPMK_ArrayIndexAdd(e,3,indices,err);

    indices[0] = newindex;
    indices[1] = ps[2];
    indices[2] = ps[0];
    IPMK_ArrayIndexAdd(e,3,indices,err);

    // change indices of the old triangle to be one of the three new triangles
    indices[0] = ps[0];
    indices[1] = ps[1];
    indices[2] = newindex;
    IPMK_ArrayIndexChange(e,p,3,indices,err);
  }
}

#include <mpi.h>
int main(int argc,char **args)
{
  IPM_Error      err;
  char           filename[128];

  IPM_Initialize(&argc,&args,&err);
  int            rank;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  snprintf(filename,128,"pdffile_%d.pdf",rank);
  cairo_surface_t *surface = cairo_pdf_surface_create(filename,400,400);
  cr = cairo_create (surface);
  cairo_scale (cr, 400, 400);
  cairo_set_line_width (cr, 1.0/200.);
  cairo_set_source_rgb (cr, 0, 0, 0);


  
  
  IPM_ArrayNumeric u = IPM_ArrayNumericCreate(&err);
  IPM_ArrayIndex elements = IPM_ArrayIndexCreate(&err);
  IPM_ArrayIndexSetOffset(elements,IPM_ArrayNumericGetOffset(u,&err),&err);    //  element indices are into the u object 
  
  IPM_Function myCreate = IPM_FunctionCreate(IPM_FUNCTION_REPARTITION,kCreate,2,IPM_MEMORY_WRITE,IPM_MEMORY_WRITE,&err);
  IPM_Launch(myCreate,(IPM_Array)u,elements,&err);

  IPM_Launch(ipm_arrayprint1,(IPM_Array)u,&err);
  IPM_Launch(ipm_arrayprint2,(IPM_Array)u,elements,&err);

  IPM_Function myRefine = IPM_FunctionCreate(IPM_FUNCTION_REPARTITION,kRefine,2,IPM_MEMORY_WRITE,IPM_MEMORY_WRITE,&err);
  IPM_Launch(myRefine,(IPM_Array)u,elements,&err);

  IPM_Launch(ipm_arrayprint1,(IPM_Array)u,&err);
  IPM_Launch(ipm_arrayprint2,(IPM_Array)u,elements,&err);

  IPM_Launch(myRefine,(IPM_Array)u,elements,&err);

  IPM_Launch(ipm_arrayprint1,(IPM_Array)u,&err);
  IPM_Launch(ipm_arrayprint2,(IPM_Array)u,elements,&err);

  IPM_Finalize(&err);
  cairo_destroy(cr);
  cairo_surface_destroy (surface);
  return 0;
}
